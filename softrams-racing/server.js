const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var hsts = require('hsts');
const path = require('path');
var xssFilter = require('x-xss-protection');
var nosniff = require('dont-sniff-mimetype');
const request = require('request');

const app = express();

app.use(cors());
app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(xssFilter());
app.use(nosniff());
app.set('etag', false);
app.use(
  helmet({
    noCache: true
  })
);
app.use(
  hsts({
    maxAge: 15552000 // 180 days in seconds
  })
);

app.use(
  express.static(path.join(__dirname, 'dist/softrams-racing'), {
    etag: false
  })
);

app.get('/api/members', (req, res) => {
  request('http://localhost:3000/members', (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

// TODO: Dropdown!
app.get('/api/teams', (req, res) => {
  request('http://localhost:3000/teams', (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

// Submit Form!
app.post('/api/addMember', (req, res) => {
  newMember = req.body;
  var clientServerOptions = {
    uri: 'http://localhost:3000/members',
    body: JSON.stringify(newMember),
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    }
}
  request(clientServerOptions, (err, response, body) => {
    if(response.statusCode <= 500) {
      console.log(response.body);
      res.send(body);
    }
  })
});

app.delete('/api/deleteMember/:id', (req, res) => {
  console.log("DELETE");
  const mId = req.params.id;
  console.log(mId);
  var clientServerOptions = {
    uri: 'http://localhost:3000/members/'+mId,
    method: 'DELETE'
}
  request(clientServerOptions, (err, response, body) => {
    if(response.statusCode <= 500) {
      console.log("ABC", body);
      res.send(body);
    }
  })
})

app.put('/api/editMember/:id', (req, res) => {
  console.log("UPDATE");
  const mId = req.params.id;
  newMember = req.body;
  console.log(mId);
  var clientServerOptions = {
    uri: 'http://localhost:3000/members/'+mId,
    method: 'PUT',
    body: JSON.stringify(newMember),
    headers: {
      'Content-Type': 'application/json'
  }
}
  request(clientServerOptions, (err, response, body) => {
    if(response.statusCode <= 500) {
      console.log("ABC", response.body);
      res.send(body);
    }
  })
})

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/softrams-racing/index.html'));
});

app.listen('8000', () => {
  console.log('Vrrrum Vrrrum! Server starting!');
});

