import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

// This interface may be useful in the times ahead...
interface Member {
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  submitted = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  memberForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    jobTitle: new FormControl('', Validators.required),
    team: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required)
  })

  constructor(private fb: FormBuilder, public appService: AppService, private router: Router) {}

  ngOnInit() {
    
  }

  ngOnChanges() {}

  displayTeamList() {
    this.appService.showTeams().subscribe( data => {
      this.teams = data;     
    })
  }

  goBack() {
    this.appService.showAddMemberForm = false;
  }

  // TODO: Add member to members
  onSubmit() {
    this.memberModel = this.memberForm.value;
    this.appService.addMember(this.memberModel).subscribe( data => {
      console.log("Added success");
    })
    this.appService.showAddMemberForm = false;
    this.showUpdatedMemberSummary();
  }
  
  showUpdatedMemberSummary() {
    console.log("reload");
    window.location.reload(true);
  }
}

