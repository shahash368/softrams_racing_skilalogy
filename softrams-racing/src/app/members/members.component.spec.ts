import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersComponent } from './members.component';
import { MemberDetailsComponent } from '../member-details/member-details.component';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { HttpClient } from '@angular/common/http';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

describe('MembersComponent', () => {
  let component: MembersComponent;
  let fixture: ComponentFixture<MembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MembersComponent, MemberDetailsComponent],
      imports: [HttpClientModule, RouterModule, FormsModule, ReactiveFormsModule],
      providers: [
        HttpClient,
        FormBuilder,
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call goToAddMemeberForm', () => {
    spyOn(component, 'goToAddMemberForm');
    component.goToAddMemberForm();
    expect(component.goToAddMemberForm).toHaveBeenCalled();
  });
  
  it('should call deleteMemberById', () => {
    spyOn(component, 'deleteMemberById');
    let id: number;
    component.deleteMemberById(id);
    expect(component.deleteMemberById).toHaveBeenCalled();
  }); 

  it('should call editChangesInMember and set showEditForm', () => {
    spyOn(component, 'editMember');
    component.editMember();
    expect(component.editMember).toHaveBeenCalled();
    expect(component.members).not.toBeNull();
    expect(component.showEditForm).toEqual(false);
  }); 

  it('should call goBack', () => {
    spyOn(component, 'goBack');
    component.goBack();
    expect(component.showEditForm).toBeFalsy();
  })

  it('should call initialMembers to update summary of members ',() => {
    spyOn(component, 'showUpdatedMemberSummary');
    component.showUpdatedMemberSummary();
    expect(component.showUpdatedMemberSummary).toHaveBeenCalled();
  }); 

});
