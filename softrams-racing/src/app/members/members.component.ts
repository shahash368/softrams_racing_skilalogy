import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];
  showEditForm = false;
  teams = [];
  id: number;
  memberEditForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    jobTitle: new FormControl('', Validators.required),
    team: new FormControl('', Validators.required),
    status: new FormControl('', Validators.required)
  })

  constructor(public appService: AppService, private router: Router) {}

  ngOnInit() {
    this.appService.showMember = true;
    this.appService.getMembers().subscribe(members => {
      this.members = members;
    });
  }

  goToAddMemberForm() {
    this.appService.showAddMemberForm = true;
  }

  editMemberByID(id: number, members) {
    this.showEditForm = true;
    this.id= id;
    this.members = members;
    this.memberEditForm.setValue({
      firstName: members.firstName,
      lastName: members.lastName,
      jobTitle: members.jobTitle,
      team: members.team,
      status: members.status
    })
  }

  displayTeamList() {
    this.appService.showTeams().subscribe( data => {
      this.teams = data;
      console.log(this.teams);
    })
  }

  goBack() {
    this.showEditForm = false;
    this.showUpdatedMemberSummary();
  }

  editMember() {
    this.showEditForm = false;
    this.members = this.memberEditForm.value;
    this.appService.editMember(this.id, this.members).subscribe(data => {
      console.log(data);
    }) 
    this.showUpdatedMemberSummary();
  }

  showUpdatedMemberSummary() {
    this.appService.getMembers().subscribe(members => (this.members = members));
  }

  deleteMemberById(id: number) {
    this.appService.deleteMember(id).subscribe(data => {
      console.log("data deleted");
    })
    this.showUpdatedMemberSummary();
  }
}
