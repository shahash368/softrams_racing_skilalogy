import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  api = 'http://localhost:8000/api';
  username: string;
  showAddMemberForm = false;
  showEditForm = false;
  members =  [];
  showMember = false;


  constructor(private http: HttpClient) {}

  // Returns all members
  getMembers() {
    return this.http
      .get(`${this.api}/members`)
      .pipe(catchError(this.handleError));
  }

  setUsername(name: string): void {
    this.username = name;
  }

  addMember(memberForm) {
    let body = memberForm;
    return this.http.post(`${this.api}/addMember`, body);
  }

  deleteMember(id: number) {
    return this.http.delete(`${this.api}/deleteMember/${id}`);
  }

  editMember(id: number, memberForm) {
    let body = memberForm;
    return this.http.put(`${this.api}/editMember/${id}`, body);
  }

  showTeams() {
    return this.http
      .get(`${this.api}/teams`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return [];
  }
}
