import { TestBed, inject } from '@angular/core/testing';

import { AppService } from './app.service';

import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule,
  HttpTestingController } from '@angular/common/http/testing';

describe('AppService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppService],
      imports: [HttpClientModule, HttpClientTestingModule]
    });
  });

  it('should be created', inject([AppService], (service: AppService) => {
    expect(service).toBeTruthy();
  }));

  it('should call get request', inject([AppService], (service: AppService) => {
    const mock = [
      {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe",
        "jobTitle": "Driver",
        "team": "Formula 1 - Car 77",
        "status": "Active"
      }
    ]

    service.getMembers().subscribe( data => {
      expect(data[0].firstName).toEqual('John');
      expect(data[0].lastName).toEqual('Doe');
      expect(data[0].jobTitle).toEqual('Driver');
      expect(data[0].team).toEqual('Formula 1 - Car 77');
      expect(data[0].status).toEqual('Active');
    })

  }));

  it('should call and get teams', inject([AppService], (service: AppService) => {
    const mock = [
      {
        "id": 1,
        "team": "Formula 1 - Car 77",
      },
      {
        "id": 2,
        "team": "Formula 1 - Car 8",
      },
      {
        "id": 3,
        "team": "Formula 1 - Car 54",
      },
      {
        "id": 4,
        "team": "Formula 1 - Car 63",
      }
    ]

    service.showTeams().subscribe( data => {
      expect(data[0].teamName).toEqual(mock[0]);
      expect(data[1].teamName).toEqual(mock[1]);
      expect(data[2].teamName).toEqual(mock[2]);
      expect(data[3].teamName).toEqual(mock[3]);
    })

  }));

  it('should set username', inject([AppService], (service: AppService) => {
    let name: string;
    let id: number;
    service.setUsername(name);
    spyOn(service, 'deleteMember');
    service.deleteMember(id);
    expect(service.deleteMember).toHaveBeenCalled();
    expect(service.username).toEqual(name);
  }))

  it('should post data', inject([AppService], (service: AppService) => {
    const mock = [
      {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe",
        "jobTitle": "Driver",
        "team": "Formula 1 - Car 77",
        "status": "Active"
      }
    ]

    service.addMember(mock).subscribe(data => {
      expect(data[0].firstName).toEqual('John')
    })
  }));
});
